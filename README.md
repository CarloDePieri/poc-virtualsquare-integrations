# Proof of Concept Virtualsquare Integrations

### Idea

Qualche giorno fa ci ha chiesto "una mano" per verificare se l'aggiornamento di
debian stesse dando problemi alla suite di virtualsquare.

Visto che continuiamo a parlare di virtualizzare pezzetti dello stack, perche'
non aggiungere un ulteriore mattoncino? 'Virtualizziamo' l'utente che fa questi test:
ho pensato di farlo con [expect](https://www.tcl.tk/man/expect5.31/expect.1.html),
che dovrebbe riuscire a riprodurre abbastanza fedelmente un umano che legge i
tutorial nella wiki e da' comandi nella shell, verificandone i risultati.

Facendo un ulteriore passo in avanti, si potrebbe automatizzare il tutto in modo
da avere test report periodici sullo stato del progetto.

### PoC

L'idea di base e' quindi di scrivere integration/system test per la suite di
virtualsquare, usando come base di partenza i tutorial presenti sulla wiki.

Un runner (nel caso di questo PoC le pipeline di GitLab (gestito da
[questo file](https://gitlab.com/CarloDePieri/poc-virtualsquare-integrations/-/blob/main/.gitlab-ci.yml)),
ma si puo' anche scegliere altro, anche se ci sono delle considerazioni da fare
in merito all'architettura) periodicamente esegue le seguenti operazioni:

 - installa nell'ambiente di test gli stessi requisiti che installerebbe un utente
 per usare la [vm di test](http://wiki.virtualsquare.org/#!tutorials/setup_the_vm.md)
 - scarica l'ultima [daily build](http://wiki.virtualsquare.org/#!/daily_brewed.md)
 (nota in fondo riguardo cio')
 - decomprime il disco qcow2 e lancia la vm
 - si connette alla vm e lancia lo script `get_v2all.sh`; poi installa `expect`
 - poi, per ogni test scritto in tcl (in questo caso solo `test.tcl`):
   - carica lo script nella vm
   - esegue lo script; se questo:
     - ritorna con successo, la suite prosegue
     - esce con un errore marka l'intera test run come fallita, poi procede con
     gli altri test
 - Alla fine segnala quali test hanno fallito, se ce ne sono

Tutto cio' risulta in una serie di [job](https://gitlab.com/CarloDePieri/poc-virtualsquare-integrations/-/pipelines)
che ci danno queste informazioni:

 - se la suite virtualsquare si installa nella daily build attuale (quindi se un
 aggiornamento di debian rompesse qualcosa di grosso lo sapremmo velocemente)
 - se i programmi della suite, almeno a spanne, stanno funzionando

Da notare: questo progetto non includerebbe unit testing, quelli sarebbero
responsabilita' dei singoli progetti.

### Lo script - test.tcl

[Lo script](https://gitlab.com/CarloDePieri/poc-virtualsquare-integrations/-/blob/main/test.tcl)
vuole simulare il [tutorial di comunicazione tra due vdens con uno
switch](http://wiki.virtualsquare.org/#!tutorials/vde_ns.md#Scenario:_two_vdens_and_a_switch):

 - lancia un processo `vde_plug`, creando uno switch virtuale
 - lancia un primo `vdens`, che configura
 - lancia un secondo `vdens`, che configura
 - pinga il primo `vdens` dal secondo
 - se riceve risposta positiva passa il test, altrimenti esce con codice di errore 1

#### TODO (?)

 - passare la wiki e scrivere i vari tutorial come test
 - scrivere una mini test suite runner che permetta di lanciare i vari test
 facilmente e ne segnali i risultati
 - usare gli snapshot della vm per resettare lo stato tra un test e l'altro
 - creare vm di partenza per piu' di una piattaforma, ad esempio debian
 stable/testing/unstable, etc e lanciare anche li' i test


#### About the daily build

Nel caso si chieda perche' non uso le daily build 'ufficiali' nel `.gitlab-ci.yml`...

Ho dovuto scaricare molte volte la daily build per testare questo poc (e in
generale per i test futuri servira' ancora); non sapendo quanto cio' disturbasse
il server e poiche' meta' delle volte la download speed stava sotto i 200kbps
per qualche motivo (forse proprio per i download ripetuti), ho automatizzato
la creazione delle daily build [qui](https://gitlab.com/CarloDePieri/virtualsquare-brewer).
