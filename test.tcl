#!/usr/bin/expect
#
# http://wiki.virtualsquare.org/#!tutorials/vde_ns.md#Scenario:_two_vdens_and_a_switch

log_user 0
set env(PATH) "/bin:/usr/bin:/usr/local/bin"

# Spawn the vde_plug switch
spawn vde_plug null:// switch:///tmp/mysw

# Spawn the first vdens 
spawn vdens vde:///tmp/mysw
set spawn_id_ns_a $spawn_id

# Configure the first vdens
expect "net_raw# "
send "ip addr add 10.0.0.10/24 dev vde0\r"
expect "net_raw# "
send "ip link set vde0 up\r"
expect "net_raw# "

# Spawn the second vdens 
spawn vdens vde:///tmp/mysw
set spawn_id_ns_b $spawn_id

# Configure the second vdens
expect "net_raw# "
send "ip addr add 10.0.0.11/24 dev vde0\r"
expect "net_raw# "
send "ip link set vde0 up\r"
expect "net_raw# "

# Send a ping from the second ns to the first
send "ping -c 1 10.0.0.10\n"
# DEBUG - using the following instead should make the test fail:
# send "ping -c 1 10.0.0.12\n"

# If ping report contains a "1 received", consider this test passed, else fails
set test_name "two vdens should be able to communicate through a virtual switch"
set timeout 3 
expect {
  timeout { puts "\[FAILED\] $test_name"; exit 1 }
  "1 received"
}
puts "\[PASSED\] $test_name"
exit
